import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    socket: {
      isConnected: false,
      data: '',
      reconnectError: false,
    },
    savegames: []
  },
  mutations:{
    SOCKET_ONOPEN (state, event)  {
      Vue.prototype.$socket = event.currentTarget
      state.socket.isConnected = true

      Vue.prototype.$socket.send(JSON.stringify({
        message_type: 'get_save_games'
      }))
    },
    SOCKET_ONCLOSE (state/*, event*/)  {
      state.socket.isConnected = false
    },
    SOCKET_ONERROR (state, event)  {
      console.error(state, event)
    },
    // default handler called for all methods
    SOCKET_ONMESSAGE (state, data)  {
      state.socket.data = data
      if (data.message_type === 'available_save_games') {
        state.savegames = data.message.save_games
      } else if (data.name === 'heartbeat') {
        console.log('heartbeat')
      }
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT(state, count) {
      console.info(state, count)
    },
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    },
  },
  actions: {
    sendMessage: function(context, message) {
      Vue.prototype.$socket.send(JSON.stringify(message))
    }
  }
})