import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueNativeSock from 'vue-native-websocket'
import store from './store'

Vue.config.productionTip = false

const target = location.hostname
// const target = '192.168.114.110'

Vue.use(VueNativeSock, 'ws://' + target + ':8765', {
  store: store,
  format: 'json',
  reconnection: true, // (Boolean) whether to reconnect automatically (false)
  reconnectionAttempts: Infinity, // (Number) number of reconnection attempts before giving up (Infinity),
  reconnectionDelay: 3000, // (Number) how long to initially wait before attempting a new (1000)
 })

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
